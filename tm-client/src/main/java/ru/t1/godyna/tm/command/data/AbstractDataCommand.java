package ru.t1.godyna.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.endpoint.IDomainEndpoint;
import ru.t1.godyna.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public IDomainEndpoint domainEndpoint;

}
