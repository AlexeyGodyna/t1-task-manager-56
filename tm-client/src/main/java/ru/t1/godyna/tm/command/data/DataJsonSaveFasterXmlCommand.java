package ru.t1.godyna.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.domain.DataJsonSaveFasterXmlRequest;
import ru.t1.godyna.tm.enumerated.Role;

@Component
public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-json";

    @Getter
    @NotNull
    private final String description = "Save data in json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        domainEndpoint.saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}
