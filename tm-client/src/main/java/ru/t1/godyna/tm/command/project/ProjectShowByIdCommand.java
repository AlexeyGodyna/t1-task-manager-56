package ru.t1.godyna.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.godyna.tm.dto.response.project.ProjectShowByIdResponse;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @NotNull
    private final String NAME = "project-show-by-id";

    @NotNull
    private final String DESCRIPTION = "Display project by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken(), id);
        @NotNull final ProjectShowByIdResponse response = projectEndpoint.showProjectById(request);
        showProject(response.getProject());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
