package ru.t1.godyna.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.service.ICommandService;
import ru.t1.godyna.tm.api.service.IPropertyService;
import ru.t1.godyna.tm.command.AbstractCommand;
import ru.t1.godyna.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
