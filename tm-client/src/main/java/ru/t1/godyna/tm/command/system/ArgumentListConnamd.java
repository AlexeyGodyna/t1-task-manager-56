package ru.t1.godyna.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentListConnamd extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "arguments";

    @NotNull
    private final String ARGUMENT = "-arg";

    @NotNull
    private final String DESCRIPTION = "Show list arguments";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (@Nullable ICommand command : commands) {
            if (command == null) continue;
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument.toString());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
