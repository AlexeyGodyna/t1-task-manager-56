package ru.t1.godyna.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.util.FormatUtil;

@Component
public final class SystemInfoCommand extends AbstractSystemCommand{

    @NotNull
    private final String NAME = "info";

    @NotNull
    private final String ARGUMENT = "-i";

    @NotNull
    private final String DESCRIPTION = "Show system information.";

    @Override
    public void execute() {
        System.out.println("[INFO]");
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int avaliableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + avaliableProcessors);
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
