package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.endpoint.ITaskEndpoint;
import ru.t1.godyna.tm.command.AbstractCommand;
import ru.t1.godyna.tm.enumerated.Role;

@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpointClient;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
