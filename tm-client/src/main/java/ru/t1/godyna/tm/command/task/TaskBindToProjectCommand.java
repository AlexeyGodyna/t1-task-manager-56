package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-bind-to-project";

    @NotNull
    private final String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken(), projectId, taskId);
        taskEndpointClient.bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
