package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.task.TaskCreateRequest;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-create";

    @NotNull
    private final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), name, description);
        taskEndpointClient.createTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
