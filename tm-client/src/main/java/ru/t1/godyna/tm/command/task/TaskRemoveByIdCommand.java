package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-remove-by-id";

    @NotNull
    private final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpointClient.removeTaskById(new TaskRemoveByIdRequest(getToken(), id));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
