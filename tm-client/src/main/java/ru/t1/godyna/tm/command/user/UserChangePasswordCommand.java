package ru.t1.godyna.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "change-user-password";

    @NotNull
    private final String DESCRIPTION = "change password of current user.";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), password);
        userEndpoint.changePassword(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
