package ru.t1.godyna.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.dto.model.UserDTO;
import ru.t1.godyna.tm.dto.request.user.UserProfileRequest;
import ru.t1.godyna.tm.dto.response.user.UserProfileResponse;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.exception.entity.UserNotFoundException;

@Component
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "view-user-profile";

    @NotNull
    private final String DESCRIPTION = "view profile of current user.";

    @Override
    public void execute() {
        @NotNull UserProfileResponse response = userEndpoint.viewProfileUser(new UserProfileRequest(getToken()));
        @Nullable final UserDTO user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
