package ru.t1.godyna.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.AbstractIdRequest;
import ru.t1.godyna.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        super(token, id);
        this.status = status;
    }

}
