package ru.t1.godyna.tm.api;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void log(@Nullable String message);

}
