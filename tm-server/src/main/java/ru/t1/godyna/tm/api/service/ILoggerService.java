package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable final String message);

    void command(@Nullable final String message);

    void error(@Nullable final Exception e);

    void initJmsLogger();

}
