package ru.t1.godyna.tm.jms.operation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    @Nullable
    private OperationType type;

    @Nullable
    private Object entity;

    @Nullable
    private String table;

    @NotNull
    private final Long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
