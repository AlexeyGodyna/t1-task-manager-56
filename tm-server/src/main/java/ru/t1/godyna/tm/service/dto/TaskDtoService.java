package ru.t1.godyna.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.godyna.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.godyna.tm.api.service.dto.ITaskDtoService;
import ru.t1.godyna.tm.comparator.NameComparator;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.TaskNotFoundException;
import ru.t1.godyna.tm.exception.field.*;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    @Override
    public ITaskDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ITaskDtoRepository.class);
    }

    @NotNull
    @Override
    public TaskDTO add(@Nullable TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            model.setUserId(userId);
            entityManager.getTransaction().begin();
            taskRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull TaskDTO task : models) {
                taskRepository.add(task);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clearAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.existsByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAllUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            if (sort == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
            return taskRepository.findAllUserId(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable Comparator<TaskDTO> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            if (comparator == null) return taskRepository.findAll(NameComparator.INSTANCE);
            return taskRepository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            if (comparator == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
            return taskRepository.findAllUserId(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findOneByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.getSizeUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO remove(@Nullable TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.remove(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeByIdUserId(userId, model.getId());
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable Collection<TaskDTO> collection) {
        if (collection == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull TaskDTO task : collection) {
                taskRepository.remove(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable TaskDTO task = findOneById(id);
            taskRepository.removeById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable TaskDTO task = findOneById(id);
            taskRepository.removeById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            clear();
            add(models);
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO update(@NotNull TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
