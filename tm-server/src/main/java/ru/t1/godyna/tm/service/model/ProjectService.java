package ru.t1.godyna.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.godyna.tm.api.repository.model.IProjectRepository;
import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.entity.StatusEmptyException;
import ru.t1.godyna.tm.exception.field.DescriptionEmptyException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.NameEmptyException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;
import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    @Override
    public Project add(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
            model.setUser(entityManager.find(User.class, userId));
            entityManager.getTransaction().begin();
            projectRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> add(@NotNull final Collection<Project> models) {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull Project project : models) {
                projectRepository.add(project);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUser(entityManager.find(User.class, userId));
        return update(project);
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clearAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.existsByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findAllUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findAllUserId(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findAllUserId(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.findOneByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            return projectRepository.getSizeUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project remove(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.remove(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project remove(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeByIdUserId(userId, model.getId());
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull Project project : collection) {
                projectRepository.remove(project);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            @Nullable Project project = projectRepository.findOneById(id);
            entityManager.getTransaction().begin();
            projectRepository.removeById(id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            @Nullable Project project = projectRepository.findOneByIdUserId(userId, id);
            entityManager.getTransaction().begin();
            projectRepository.removeByIdUserId(userId, id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> set(@NotNull final Collection<Project> collections) {
        if (collections == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            clear();
            add(collections);
            entityManager.getTransaction().commit();
            return collections;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project update(@NotNull final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUser(entityManager.find(User.class, userId));
        @NotNull final IProjectRepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
